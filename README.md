# TouchStore connector

TouchStore connector zajišťuje obousměrnou komunikaci mezi expedičním systémem TouchStore (dále jen TS)
a IS zákazníka (obvykle eshop, účetní SW, ERP systém, dále jen IS). Connector je implementován jako JSON REST API.

Komunikace se skládá z:

| Název       | Metoda | Povinné | Název v TS                | Popis                                                                                               |
|-------------|--------|---------|---------------------------|-----------------------------------------------------------------------------------------------------|
| orders      | GET    | Ano     | Stažení objednávek        | export dat z IS do TS (objednávky či faktury určené k expedici, dále jen objednávka)                |
| downloaded  | POST   | Ne      | Hebhook: stažení          | odeslání informací o uložení objednávek do TouchStore (proběhne po stažení do TouchStore)           |
| error       | POST   | Ano     | Webhook: Chyba            | odeslání chyby vztažené k objednávce z TS do IS (neúplná adresa, požadované zboží není skladem,...) |
| package-end | POST   | Ne      | Webhook: Konec balení     | notifikace o zabalení objednávky (proběhne po vytištění štítku)                                     |
| send        | POST   | Ano     | Webhook: Předání dopravci | odeslání informací o vytvořených zásilkách (proběhne po vytvoření svozu)                            |

V TS jsou definované URL adresy pro jednotlivé endpointy.

Příklad zadání URL endpointů do TouchStore včetně HTTP Basic Auth:

![data source add](./res/data-source-add.png)

## Orders: export dat do systému TouchStore

TS každých **5 minut kontaktuje connector** a následně aktualizuje data ve své databázi. Pokud nalezne novou objednávku,
vloží ji do své databáze. Pokud nalezne objednávku, kterou má ve své databázi a není z ní vytvořená zásilka, tak ji aktualizuje.

Exportujte pouze objednávky, které jsou určené k odeslání přepravcem
(obvykle se rozpoznává, podle položky v objednávce - např. Doprava DPD, nebo podle typu dopravy v hlavičce objednávky, **neexportujte** objednávky jejichž položky nejsou skladem).

Pokud nejste schopni určit neodeslané objednávky (např. **stav objednávky = k expedici**), exportujte objednávky, které byly vytvořené (či změněné) za posledních 14 dní).

Konektor je vhodné zabezpečit přes **HTTPS a HTTP BASIC AUTH**.

### Request

```
GET /touchstore/orders HTTP/1.1
Host: www.example.com
```

### Response

TouchStore očekává response ve formátu JSON (`Content-Type: application/json`). V odpovědi jsou příslušné objednávky v poli.

```json
[
    {
        "order_id": "326d7fa9-b2eb-47c4-987c-fed6ffd2d368",
        "carrier": "ppl",
        "carrier_product": "private",
        "carrier_branch_id": null,
        "extra_branch_id": null,
        "priority": 4,
        "recipient_name": "Karel Novák",
        "recipient_contact": null,
        "recipient_street": "Borovy 63",
        "recipient_city": "Borovy",
        "recipient_state": "",
        "recipient_zip": "33401",
        "recipient_country_code": "CZ",
        "recipient_phone": "+420606123456",
        "recipient_email": "info@touchstore.cz",
        "weight": 3.5,
        "ic": null,
        "dic" : null,
        "note": "interni poznamka",
        "driver_note": "poznamka pro ridice",
        "description": "povleceni",
        "services": [],
        "ref": "202000153",
        "label": "202000153",
        "barcode": "202000153",
        "cod_vs": "202000153",
        "cod_amount": 1294.00,
        "cod_currency": "CZK",
        "cod_card_payment": true,
        "ins_amount": 1294.00,
        "ins_currency": "CZK",
        "date_delivery": "2016-03-05 10:29:40",
        "date_source": "2016-03-04 10:29:40",
        "products": [
            {
                "item_id": "326d7fa9",
                "sku": "MD120",
                "name": "Modrá deka 120 cm",
                "ean": "8693495033329",
                "qty": 3,
                "image_url": "https:\/\/example.com\/image\/deka.jpg",
                "unit": "ks",
                "location": "M1/A2/44",
                "note": "poznámka k položce"
            },
            {
                "item_id": "fed6ffd2d368",
                "sku": "CD140",
                "name": "Černá deka 140 cm",
                "ean": "",
                "qty": 1,
                "unit": "ks",
                "location": "M1/B1/03"
            }
        ],
        "attachments": [
            {
                "name": "faktura",
                "href": "https:\/\/example.com\/faktura?order_id=326d7fa9-b2eb-47c4-987c-fed6ffd2d368"
            },
            {
                "name": "dodaci list",
                "href": "https:\/\/example.com\/dodaci-list?order_id=326d7fa9-b2eb-47c4-987c-fed6ffd2d368"
            }
        ]
    }
]
```

### Objednávka má následující atributy

* order_id (vaše interní id objednávky - cart.id) - povinné, datový typ string
* carrier (dopravce) - povinný údaje. nabývá hodnot:
    - cpost
    - dhl-express
    - dhl-freight
    - dpd
    - dpd-sk (smlouva s DPD SK)
    - express-one-hu
    - geis
    - gibon
    - gls
    - gls-hu (smlouva s GLS HU)
    - gls-sk (smlouva s GLS SK)
    - intime (WeDo)
    - personal
    - pbh (pošta bez hranic)
    - ppl 
    - sk-post
    - sps-sk
    - toptrans
    - ups
    - urgent-cargus-ro
    - zasilkovna
    - zasilkovna-sk (smlouva s Packeta SK)
* carrier_product (typ zásilky)
    - cpost: `RR` (rychlá zásilka), `DE` (do ruky), `DR` (do ruky), `DV` (do vlastních rukou), `NP` (na poštu), `NB` (balíkovna), `BE` (balík express), `BN` (balík nadrozměr) - nepovinné
    - dhl-express: `N` (Domestic express DOC), `H` (Economy select 12:00), `P` (Express worldwide)
    - dhl-freight: `ECD_B2B` (EuroConnect Domestic B2B), `ECD_B2C` (EuroConnect Domestic B2C), `ECI` (Transportation of international piece shipments)
    - dpd: nepovinné, TS rozpozná typ automaticky. Nicméně je možné nastavit typ ručně (`classic`, `private` nebo `parcel` pro doručení na parcelshop).
    - dpd-sk: `1` (DPD Classic), `17` (doručení na parcelshop).
    - express-one-hu: `24H` (Standard delivery - doručení na adresu v HU), `D2S` (Delivery to Parcel Point - doručení na výdejní místo), `EXP` (Export shipment - doručení na adresu mimo HU)
    - geis: (`cargo`) - nepovinné (pokud není uvedeno, TS automaticky přiřadí `cargo`)
    - gibon: neuvádí se - prázdný string nebo null
    - gls (gls-hu, gls-sk): `parcelshop` nebo `business` (pokud není uvedeno nebo je prázdné TS automaticky přiřadí `business`)
    - intime: `AUTO` (Intime API určí typ automaticky), `PUP` (wedo point), `BOX` (wedo box), `S-24-CZ`, `M-24-CZ`, `L-24-CZ`, `XL-24-CZ`, `2XL-24-CZ`, `L-48-SK`, `2XL-48-SK`, `EU_PARCEL` (pokud není uvedeno nebo je prázdné TS automaticky přiřadí `AUTO`)
    - personal: neuvádí se - prázdný string nebo null
    - pbh: neuvádí se - prázdný string nebo null
    - ppl: `business` (B2B v CZ), `private` (B2C v CZ), `cz_smart` (parcelshop v CZ), `smart_europe` (parcelshop mimo CZ), `connect` (B2C mimo CZ), `connect_plus` (B2B mimo CZ) - nepovinné. TS rozpozná typ automaticky, pokud není uveden.
    - sk-post: `R` (doporučený list), `ZBA` (zmluvný balík na adresu), `ZBP` (zmluvný balík na poštu), `BA` (balík na adresu), `BP` (balík na poštu), `EKA` (expres kuriér na adresu), `EKP` (expres kuriér na poštu)
    - sps-sk: `expres` (balík do SK), `export` (balík mimo SK), `expres_2PT` (balíkomat v SK), `expres_2PS` (parcelshop v SK), `export_2PT` (balíkomat mimo SK), `export_2PS` (parcelshop mimo SK)
    - toptrans: `T` (Standard), `I` (Toptime), `P` (Top-privat), `W` (Top-weekend), `O` (Osobní odběr) 
    - ups: `08` (UPS Expedited), `07` (UPS Express), `11` (UPS Standard), `54` (UPS Worldwide Express Plus), `65` (UPS Worldwide Saver)
    - urgent-cargus-ro: `standard`
    - zasilkovna (zasilkovna-sk): `place` (pokud není uvedeno nebo je prázdné TS automaticky přiřadí place)
* carrier_branch_id (id výdejního místa) - např. psč pro ČP balík na poštu nebo do balíkovny nebo id výdejního místa pro Uloženka, Zásilkovna, Intime (wedo point a wedo box), PPL Parcelshop atd...
* extra_branch_id (extra id výdejního místa) - pro Zásilkovnu a dopravce s pickup pointy a boxy
* services (pole string hodnot - služky k zásilce - nepovinné - pokud žádné služby nejsou, uvádějte prázdné pole)
    - gibon: `EveningDelivery` (večerní doručení)
* priority (priorita - čím vyšší číslo, tím bude objednávka umístěna výše) - nepovinné
* recipient_name (jméno / název firmy) - povinné
* recipient_contact (kontaktní údaje - např. jméno osoby při doručení do firmy) - nepovinné
* recipient_street (ulice) - povinné, pokud se nedoručuje na pobočku (parcelshop)
* recipient_city (město) - povinné, pokud se nedoručuje na pobočku (parcelshop)
* recipient_state (stát/okres) - povinné pro UPS - US, CA a Pošta bez hranic - RO, jinak nemusíte uvádět (případně prázný string nebo null)
* recipient_zip (psč - udávejte bez mezer) - povinné, pokud se nedoručuje na pobočku (parcelshop)
* recipient_country_code (stát ve formátu dle [ISO 3166-1 alpha 2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2), např. "CZ", "SK") - povinné
* recipient_phone (mobilní telefonní číslo - udávejte bez mezer a v mezinárodním formátu +420123456789) - nepovinné, ale doporučené
* recipient_email (emailová adresa) - nepovinné, ale doporučené
* weight (předpokládaná hmotnost v kg, 2 desetinná místa) - nepovinné
* ic (IČO příjemce) - nepovinné
* dic (DIČ příjemce) - nepovinné
* note (interní poznámka) - nepovinné
* driver_note (poznámka pro řidiče) - nepovinné
* description (popis zboží v objednávce) - nepovinné
* ref (označení objednávky pro dopravce - tiskne se na štítek) - nepovinné, pokud není uvedené, použije se order_id
* label (označení objednávky v TouchStore) - nepovinné, pokud není uvedené, použije se order_id
* barcode (čárový kód - pokud se objednávky hledají čtečkou čárových kódů) - nepovinné, pokud není uvedené, použije se order_id
* cod_amount (doběrečná částka, 2 desetinná místa, pokud objednávka není na dobírku, uvádějte 0)
* cod_currency (měna dobírky ve formátu [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217), např. "CZK", "EUR") - uvádějte vždy
* cod_vs (variabilní symbol) - uvádějte vždy
* cod_card_payment (platba kartou - true nebo false) - nepovinné, defaultní hodnota je false
* ins_amount (výše pojištění/hodnota zboží v zásilce, 2 desetinná místa) - povinné
* ins_currency (měna pojištění ve formátu [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217), např. "CZK", "EUR") - povinné
* date_delivery (datum doručení YYYY-MM-DD HH:MM:SS) - nepovinné, datum doručení - datum plánovaného předání zásilky zákazníkovi dopravcem
* date_source (datum poslední změny ve formátu YYYY-MM-DD HH:MM:SS) - nepovinné, ale doporučené
* products (pole položek, které se mají odeslat, pořadí v TS je stejné jako pořadí ve feedu) - nepovinné, ale doporučené
    - item_id (vaše interní "databázové" id položky v objednávce - cart_item_id, nejedná se o id produktu. item_id musí být unikátní pro všechny objednávky. pokud není id k dispozici, lze ho složit například jako order_id + pořadí položky v objednávce. může být int nebo string) - nepovinné, ale doporučené
    - sku (objednací číslo) - nepovinné
    - ean (čárový kód) - nepovinné
    - name (název) - povinné
    - qty (počet - celé číslo) - povinné
    - image_url (url adresa ke stažení obrázku ve formátu jpg, png nebo gif - žádné HTTP AUTH) - nepovinné
    - weight (předpokládaná hmotnost v kg, 2 desetinná místa) - nepovinné
    - location (skladová pozice) - nepovinné
    - unit (měrná jednotka) - nepovinné
    - note (poznámka) - nepovinné
* attachments (pole příloh - dokumenty určené k tisku) - nepovinné
    - name (název) - nepovinné
    - href (url adresa ke stažení přílohy v pdf) - povinné

Do atributů ref, label a barcode se obvykle uvádí číslo objednávky z eshopu.

### Webhook downloaded - Jak nepřenášet zbytečně stejná data 

Jak bylo popsáno výše, systém TouchStore každých pět minut stahuje data o objednávkách určených k expedici (v eshopu jsou objednávky například ve stavu `k expedici`).
Což zbytečně zatěžuje server a vytváří traffic.

Elegantní řešení jak tento problém vyřešit, je zpracování webhooku downloaded. Po stažení objednávek TouchStore zavolá POST request, který obsahuje zpracované objednávky.
Na straně konektoru by typicky mělo dojít ke změně stavu na: `probíhá expedice`.

### Request

```
POST /api/touchstore/downloaded HTTP/1.1
Host: example.com
Content-Type: application/json

{
  "items": {
    "skipped": [],
    "same": [],
    "inserted": [
      {
        "order_id": "326d7fa9-b2eb-47c4-987c-fed6ffd2d368"
      }
    ],
    "updated": []
  }
}

```

## WebHook Send: předání zásilek dopravci (tzv. svoz)

Po vytvoření svozu (tj. předání zásilek řidiči) TS kontaktuje connector a předá mu informace o vytvořených zásilkách. Tato funkce není povinná.

Obvyklé využití:

- uložení trackovacích čísel vytvořených zásilek k objednávkám
- odeslání notifikačních emailů zákazníkům
- odeslané objednávky se přestanou exportovat znovu do TS (doporučujeme)

### Request

```
POST /api/touchstore/send HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "list_id": 15,
    "carrier": "CPOST",
    "consignments": [
        {
            "order_id": "250028",
            "product": "NP",
            "track_ids": [
                "DV4241930002M",
                "DV4241930016M"
            ],
            "track_url": "https://www.postaonline.cz/trackandtrace/-/zasilka/cislo?parcelNumbers=DV4241930002M",
            "email": "info@touchstore.cz",
            "phone": "+420606123456",
            "branch_id": "30100",
            "ref": "12345",
            "label": "123456",
            "barcode": "OB12345"
        }
    ]
}
```

### Svoz má následující atributy

* list_id (číslo svozu)
* carrier (id dopravce)
* consignments (pole zásilek)
    - order_id (ID objednávky) - je prázdné, pokud byla zásilka vytvořená ručně
    - product (typ zásilky)
    - track_ids (pole trackovacích čísel - pokud jich je uvedeno více než 1, tak to znamená, že bylo vytvořeno více balíků)
    - track_url (odkaz pro sledování zásilky na webu dopravce)
    - email (emailová adresa)
    - phone (telefonní číslo)
    - branch_id (id pobočky pokud se jedná o doručení na pobočku)
    - ref (reference zákazníka - označení objednávky - pro dopravce)
    - label (označení objednávky v TouchStore)
    - barcode (čárový kód)

## Error: odeslání chyby

V TS je možné objednávku označit chybou. Tyto chyby jsou uživatelsky definovatelné v systému TS.
 
Zákazníkům doporučujeme tyto chybové stavy:

- zboží není skladem
- neplatná adresa
- ostatní

Pokud je objednávka označená jako chybná, odešle se request do connectoru a objednávka se v TS přestane zobrazovat, dokud nedojde k opravě objednávky.
Systém TS objednávku automaticky znovu zobrazí, pokud dojde k úpravě/opravě v IS
(technicky: porovnává se hash původní objednávky s hashem objednávky v exportu - tento hash se tvoří v TS na základě atributů v objednávce). 

V obvyklé implementaci connectoru se zašle email pracovníkovi IS, který může chybu opravit nebo stanovit další postup s objednávkou.

### Request

```
POST /api/touchstore/error HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "order_id": "150001",
    "error_id": "zbozi",
    "error_name": "zboží není skladem",
    "error_note": "text dopsany obslouhou systemu TS"
}
```

### Chyba má následující atributy

- order_id (ID objednávky)
- error_id (ID chyby - nastavuje se v TS)
- error_name (název chyby - nastavuje se v TS)
- error_note (poznámka k chybě - obsluha TS může vyplnit poznámku)

## Package-end: objednávka je zabalená (tj. byl vytištěný štítek)

Po vytvoření štítku může TS notifikovat connector. Pokud používáte stavy objednávky, můžete změnit např. na `Zabaleno`.

### Request

```
POST /api/touchstore/package-end HTTP/1.1
Host: example.com
Content-Type: application/json

{
    "order_id": "150001",
    "date_time": "2017-12-23T10:10:00"
}
```
